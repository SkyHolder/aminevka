# README

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for?

It is empty theme for October CMS.

Use it like start point of your amazing site

### How do I get set up?

just clone it in /themes/YourNameOfTheme/

and make it theme by default, all usefull things stay right on main page

this theme contain:

*   12-columns responsive grid (also support flex-grid and simple customize col count)
*   slick slider by[](http://kenwheeler.github.io/slick/)
*   css reset by[](http://meyerweb.com/eric/tools/css/reset/)
*   modal
*   tabs
*   menu
*   set of buttons
*   nice LESS icon generator

Contained files:

*   assets
    *   css
        *  app.less - for your amazimg styles
        *  framework.less - grid, helpers, and other
        *  medium.less - style for medium device
        *  reset.css - small style reset
        *  slick-theme.less - style for slider
        *  slick.less - style for slider
        *  small.less - style for medium device
        *  vars.less - base variables
    *   fonts
    *   img
    *   js
*   layouts
*   pages
*   partials
    *   footer - empty sample of footer
    *   head - contain <head> tag foe style meta and other
    *   header - empty sample of footer
    *   modals - all modals must be here ( or not, as you wish )
    *   scripts - scripts file arrow for combiner
    *   styles - styles file arrow for combiner

### Who do I talk to?

I am [Kurteev Nikolay](https://vk.com/skyholder), and it my favorite repository